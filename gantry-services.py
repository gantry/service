# LXC Python Library
# for compatibility with LXC 0.8 and 0.9
# on Ubuntu 12.04/12.10/13.04

# Author: Gantry Team
# Contact: -

# The MIT License (MIT)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# This permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import techworks.drivers.LxcDriver as lxc
import techworks.drivers.SystemDriver as sysInfo


# # BEGIN Tornado =====================================
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
# from yourapplication import app

import tornado.ioloop
import tornado.web

import sockjs.tornado
from sockjs.tornado.router import SockJSRouter

import os
import threading
import json
import datetime


# Jinja2 Template Processing
from tornado_jinja2 import Jinja2Loader
from jinja2 import Environment, FileSystemLoader
jinja2_env = Environment(loader=FileSystemLoader('./techworks'))
jinja2_loader = Jinja2Loader(jinja2_env)


# Database ORM Driver
from mongoengine import *
connect('mongoengine_test', host='localhost', port=27017)

# Low-Level Mongo Access
from techworks.drivers.MongoDriver import MongoDriver

# DevOps Drivers
from techworks.drivers.VagrantDriver import VagrantDriver
from techworks.drivers.AnsibleDriver import AnsibleDriver
from techworks.drivers.TerraformDriver import TerraformDriver

# Services
from techworks.services.alfresco.AlfrescoService import AlfrescoService
from techworks.services.gitlab.GitLabService import GitLabService
from techworks.services.gitlab.CommitService import CommitService
from techworks.services.gitlab.IssueService import IssueService
from techworks.services.gitlab.BroadcastMessageService import BroadcastMessageService
from techworks.services.gitlab.GroupService import GroupService
from techworks.services.gitlab.ProjectService import ProjectService
from techworks.services.compute.LxdService import LxdService
from techworks.services.nginx.NginxService import NginxService
from techworks.services.auth.LdapService import LdapService

# Interfaces
from techworks.services.BaseHandler import BaseHandler
from techworks.services.ansible.PlaybookListService import PlaybookListService
from techworks.services.vagrant.VagrantCommandService import VagrantCommandService
from techworks.services.ansible.PlaybookExecuteService import PlaybookExecuteService

# Universal WebSocket
from techworks.services.websocket.WebSocketConnection import WebSocketConnection


# # Model
# from techworks.model.Playbook import Playbook


ansible = AnsibleDriver()
terraform = TerraformDriver()
mongo = MongoDriver()
#
# alfresco = AlfrescoService()
# gitlab = GitLabService()



bootMessage = """

Welcome to Gantry Services
http://localhost:8080/

Enjoy,

:-j
"""


###################################################
# Tornado
###################################################

if __name__ == '__main__':

    import logging
    logging.getLogger().setLevel(logging.DEBUG)

    # import json
    # with open('settings.json', 'r') as f:
    #     config = json.load(f)
    #
    # secret_key = config['DEFAULT']['SECRET_KEY']  # 'secret-key-of-myapp'
    # ci_hook_url = config['CI']['HOOK_URL']  # 'web-hooking-url-from-ci-service'
    #
    # print(config)

    # Tornado URLS
    handlers = [
        # (r"/", IndexHandler),
        (r"/playbookList", PlaybookListService),
        (r"/vagrantCommand", VagrantCommandService),
        (r"/playbookExecute", PlaybookExecuteService),
        (r"/gitlab/project", ProjectService),
        (r"/gitlab/commit", CommitService),
        (r"/gitlab/issue", IssueService),
        (r"/gitlab/message", BroadcastMessageService),
        (r"/gitlab/group", GroupService),
        (r"/compute/lxd", LxdService),
        (r"/proxy/nginx", NginxService),
        (r"/auth/ldap", LdapService),
    ]

    # Tornado WehSocket
    ChatRouter = SockJSRouter(WebSocketConnection, '/chat')
    # connection = ChatRouter.get_connection_class()

    # Tornado configuration
    settings = dict(
        template_loader=jinja2_loader,
        static_path=os.path.join(os.path.dirname(__file__), "./techworks"),
        debug=True
    )
    tornadoApp = tornado.web.Application(handlers + ChatRouter.urls, **settings)


    # Tornado Server
    # http://flask.pocoo.org/docs/0.10/deploying/wsgi-standalone/
    print ("Launching Tornado Server")
    tornadoApp.listen(8080)
    print (bootMessage)

    # Run Flask Server inside Tornado as WSGIContainer
    # Literally launch our own ESB !!-]
    # Start your Servers :-j
    IOLoop.instance().start()
