import os
from ansible.plugins.callback import CallbackBase

import json

from techworks.services.websocket.WebSocketConnection import WebSocketConnection


try:
    from __main__ import display as global_display
except ImportError:
    from ansible.utils.display import Display
    global_display = Display()


# Ansible Logging Callback
# See python API : ansible.plugins.callback.CallbackBase
#
# Overrides default shell output enabling redirection
# In this example, we dump Ansible output to stdout
# We also convert output to json & pipe to a SockJS WebSocket for upstream WebApp consumption
class AnsibleCallbackWebSocket(CallbackBase):

    def __init__(self, display=None):
        print("\n\n=== ResultAccumulator : __init__ ===================================")
        self.playbook = None
        self.playbook_name = None
        self.play = None
        self.task = None

        if display:
            self._display = display
        else:
            self._display = global_display

        self._display.verbosity = 3
        if self._display.verbosity >= 4:
            name = getattr(self, 'CALLBACK_NAME', 'unnamed')
            ctype = getattr(self, 'CALLBACK_TYPE', 'old')
            version = getattr(self, 'CALLBACK_VERSION', '1.0')
            self._display.vvvv('Loaded callback %s of type %s, v%s' % (name, ctype, version))

        print(self.playbook)
        print(self.playbook_name)
        print(self.play)
        print(self.task)


    def printPretty(self, json_obj):
        parsed = json.loads(json_obj)
        msg = json.dumps(parsed, indent=4, sort_keys=True, ensure_ascii=False)
        print (msg)
        return msg

    def printPrettyObj(self, python_obj):
        msg = json.dumps(python_obj, indent=4, sort_keys=True, ensure_ascii=False)
        print (msg)
        return msg

    def sendWebSocketMessage(self, command, result):
        print("\n=== ResultAccumulator : " + command)
        print("Sending Websocket")
        msg = self.printPretty(self._dump_results(result._result))
        for connection in WebSocketConnection.participants:
            connection.sendMessage(command + msg)
        print("Transmitted Websocket")

    def sendWebSocketObject(self, command, python_obj):
        print("\n=== ResultAccumulator : " + command)
        print("Sending Websocket")
        msg = self.printPrettyObj(python_obj)
        for connection in WebSocketConnection.participants:
            connection.sendMessage(command + msg)
        print("Transmitted Websocket")




    # def v2_on_any(self, *args, **kwargs):
        # print("\n=== ResultAccumulator : v2_on_any")
        # self._display.display('%s: %s: %s' % (self.playbook_name,self.play.name, self.task))
        # print(self.playbook_name)
        # print(self.play.name)
        # print(self.task)
        # print("Args 0: ")
        # print(*args)
        # print("Args 5: ")
        # print(kwargs)
        # print("Args 7: ")
        # print(**kwargs)


    def v2_playbook_item_on_failed(self, result):
        print("\n=== ResultAccumulator : v2_playbook_item_on_failed")
        delegated_vars = result._result.get('_ansible_delegated_vars', None)
        if 'exception' in result._result:
        # Extract the error message and log it
            error = result._result['exception'].strip().split('\n')[-1]
            print(error)

            # Remove the exception from the result so it's not shown every time
            del result._result['exception']

        if delegated_vars:
            print("failed: [%s -> %s] => (item=%s) => %s" % (result._host.get_name(), delegated_vars['ansible_host'], result._result['item'], self._dump_results(result._result)))
        else:
            print("failed: [%s] => (item=%s) => %s" % (result._host.get_name(), result._result['item'], self._dump_results(result._result)))

    def v2_runner_on_failed(self, result, ignore_errors=False):
        command = "v2_runner_on_failed"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_ok(self, result):
        command = "v2_runner_on_ok"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_skipped(self, result):
        command = "v2_runner_on_skipped"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_unreachable(self, result):
        command = "v2_runner_on_unreachable"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_no_hosts(self, task):
        command = "v2_runner_on_no_hosts"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_async_poll(self, result):
        command = "v2_runner_on_async_poll"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_async_ok(self, result):
        command = "v2_runner_on_async_ok"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_async_failed(self, result):
        command = "v2_runner_on_async_failed"
        self.sendWebSocketMessage(command, result)

    def v2_runner_on_file_diff(self, result, diff):
        command = "v2_runner_on_file_diff"
        self.sendWebSocketMessage(command, result)

    def v2_playbook_on_start(self, playbook):
        command = "v2_playbook_on_start"
        self.playbook = playbook
        self.playbook_name = os.path.basename(self.playbook._file_name)
        print(self.playbook)
        print(self.playbook_name)

    def v2_playbook_on_notify(self, result, handler):
        command = "v2_playbook_on_notify"
        self.sendWebSocketMessage(command, result)
        print(handler)

    def v2_playbook_on_no_hosts_matched(self):
        command = "v2_playbook_on_no_hosts_matched"

    def v2_playbook_on_no_hosts_remaining(self):
        command = "v2_playbook_on_no_hosts_remaining"

    def v2_playbook_on_task_start(self, task, is_conditional):
        command = "v2_playbook_on_task_start"
        self.task = task
        print(self.task)
        print(self.task._uuid)
        print(self.task.get_name().strip())
        print(self.task.get_path())
        print(self.task.args.items())
        self.sendWebSocketObject(command, self.task.args)

        # print("TASK [%s]" % task.get_name().strip())

    def v2_playbook_on_cleanup_task_start(self, task):
        command = "v2_playbook_on_cleanup_task_start"
        print(task)

    def v2_playbook_on_handler_task_start(self, task):
        command = "v2_playbook_on_handler_task_start"
        print(task)

    def v2_playbook_on_vars_prompt(self, varname, private=True, prompt=None, encrypt=None, confirm=False, salt_size=None, salt=None, default=None):
        command = "v2_playbook_on_vars_prompt"

    def v2_playbook_on_setup(self):
        command = "v2_playbook_on_setup"

    def v2_playbook_on_import_for_host(self, result, imported_file):
        command = "v2_playbook_on_import_for_host"
        self.sendWebSocketMessage(command, result)

    def v2_playbook_on_not_import_for_host(self, result, missing_file):
        command = "v2_playbook_on_not_import_for_host"
        self.sendWebSocketMessage(command, result)

    def v2_playbook_on_play_start(self, play):
        command = "v2_playbook_on_play_start"
        print(self.play)
        self.play = play
        name = play.get_name().strip()
        if not name:
            msg = "PLAY"
        else:
            msg = "PLAY [%s]" % name
        print(msg)


    def v2_playbook_on_stats(self, stats):
        command = "v2_playbook_on_stats"
        run_success = True
        hosts = sorted(stats.processed.keys())
        for h in hosts:
            t = stats.summarize(h)
            print("Stat Summary : ")
            self.sendWebSocketObject(command, t)
            # self.printPretty(t)
            if t['unreachable'] > 0 or t['failures'] > 0:
                run_success = False

        if(run_success):
            print("Success !!!")
        else:
            print("Failed !!!")

    def v2_on_file_diff(self, result):
        command = "v2__on_file_diff"
        self.sendWebSocketMessage(command, result)

    def v2_playbook_on_include(self, included_file):
        command = "v2_playbook_on_include"
        print(included_file)

    def v2_runner_item_on_ok(self, result):
        command = "v2_runner_item_on_ok"
        self.sendWebSocketMessage(command, result)


    def v2_runner_item_on_failed(self, result):
        command = "v2_runner_item_on_failed"
        self.sendWebSocketMessage(command, result)

    def v2_runner_item_on_skipped(self, result):
        command = "v2_runner_item_on_skipped"
        self.sendWebSocketMessage(command, result)

    def v2_runner_retry(self, result):
        command = "v2_runner_retry"
        self.sendWebSocketMessage(command, result)
