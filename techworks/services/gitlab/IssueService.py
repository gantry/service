#!/usr/bin/python3

import json
import simplejson

import gitlab
from techworks.services.gitlab.GitLabService import GitLabService

import tornado.web
import tornado.escape
import tornado.httpclient

# ============================================
# Service which proxies calls to GitLab Commit API
# ============================================
class IssueService(GitLabService) :



    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            hostsFile = data["hostsFile"]
            print ("Hosts File")
            print (hostsFile)
            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:

            print("\n\n====================== Auth ======================")
            # username/password authentication (for GitLab << 10.2)
            # gl = gitlab.Gitlab('http://13.59.58.199/gitlab', email='root', password='d3v0lut10n')
            gl = gitlab.Gitlab('http://13.59.58.199/gitlab', private_token='NpsUxU6TamQB7GzgCYyu')

            # make an API request to create the gl.user object. This is mandatory if you use the username/password authentication.
            gl.auth()


            print("\n\n====================== Projects ======================")
            # Get list all the projects
            projects = gl.projects.list()

            # Get 1st Project
            project = gl.projects.get(11)

            for projectitem in projects:
                print(projectitem)
            #     print(projectitem.attributes)
            #     print(projectitem.issues)
            #     print(projectitem.issues)
            #     print(projectitem.repository_tree)

            print("\n\n====================== Issues ======================")
            issues = []
            for issue in project.issues.list():
                print(issue)
                issues.append(issue.attributes)

            issuedict = {"issues":issues}
            print(json.dumps(issuedict, indent=4, sort_keys=True))

            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                issues=issuedict,
                test="test message"
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()



        except:
            self.write({"success":False})



