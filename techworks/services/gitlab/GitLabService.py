#!/usr/bin/python3

import json
import simplejson

import gitlab
from techworks.services.BaseHandler import BaseHandler

import tornado.web
import tornado.escape
import tornado.httpclient

# ============================================
# Service which proxies calls to GitLab API
# ============================================
class GitLabService(BaseHandler) :

    gitlabURL = "http://13.59.58.199/gitlab";
    gitlabPrivateToken = "NpsUxU6TamQB7GzgCYyu"
    gitlabUser = "root"
    gitlabPassword = "d3v0lut10n"


    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            hostsFile = data["hostsFile"]
            print ("Hosts File")
            print (hostsFile)
            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:

            print("\n\n====================== Auth ======================")
            # username/password authentication (for GitLab << 10.2)
            # gl = gitlab.Gitlab(self.gitlabURL, email=root, password=d3v0lut10n)
            gl = gitlab.Gitlab(self.gitlabURL, private_token=self.gitlabPrivateToken)

            # make an API request to create the gl.user object. This is mandatory if you use the username/password authentication.
            gl.auth()

            print("\n\n====================== Broadcast Messages ======================")
            broadcastmessages = gl.broadcastmessages.list()
            print(broadcastmessages)

            # Create Message
            msg = gl.broadcastmessages.create({'message': 'Important information soon'})


            print("\n\n====================== Groups ======================")
            groups = gl.groups.list()
            for group in groups:
                print(group)


            print("\n\n====================== Projects ======================")
            # Get list all the projects
            projects = gl.projects.list()

            # Get 1st Project
            project = gl.projects.get(1)

            for projectitem in projects:
                print(projectitem)
            #     print(projectitem.attributes)
            #     print(projectitem.commits)
            #     print(projectitem.issues)
            #     print(projectitem.repository_tree)

            print("\n\n====================== Attributes ======================")
            attributes = project.attributes
            print(project.attributes)
            print(json.dumps(project.attributes, indent=4, sort_keys=True))
            # for key, value in attributes.items():
            #     print("{} = {}".format(key, value))

            print("\n\n====================== Repository Tree ======================")
            items = project.repository_tree()
            print(json.dumps(items, indent=4, sort_keys=True))
            for item in items:
                print(json.dumps(item, indent=4, sort_keys=True))
                # for key, value in item.items():
                #     print("{} = {}".format(key, value))

            print("\n\n====================== Users ======================")
            users = project.users.list()
            for user in users:
                # print(user)
                print(json.dumps(user.attributes, indent=4, sort_keys=True, skipkeys=True, ensure_ascii=True, check_circular=True))

            print("\n\n====================== Members ======================")
            members = project.members.list()
            for member in members:
                print(member)

            print("\n\n====================== Languages ======================")
            languages = project.languages()
            for language in languages:
                print(language)


            print("\n\n====================== Issues ======================")
            issues = project.issues.list()
            for issue in issues:
                # print(issue.__dict__)
                print(json.dumps(issue.attributes, indent=4, sort_keys=True, skipkeys=True, ensure_ascii=True, check_circular=True))
            print(issues)

            # print("\n\n====================== Commits ======================")
            # commits = []
            # for commit in project.commits.list():
            #     print(commit)
            #     # commits.append(commit._attrs)
            #     commits.append(commit.attributes)
            #
            # commitdict = {"commits":commits}
            # print(json.dumps(commitdict, indent=4, sort_keys=True))

            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                attributes=attributes,
                test="test message"
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()


        except:
            self.write({"success":False})



