#!/usr/bin/python3

import json
import simplejson

import gitlab
from techworks.services.gitlab.GitLabService import GitLabService

import tornado.web
import tornado.escape
import tornado.httpclient

# ============================================
# Service which proxies calls to GitLab API
# ============================================
class GroupService(GitLabService) :



    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            hostsFile = data["hostsFile"]
            print ("Hosts File")
            print (hostsFile)
            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:

            print("\n\n====================== Auth ======================")
            # username/password authentication (for GitLab << 10.2)
            # gl = gitlab.Gitlab('http://13.59.58.199/gitlab', email='root', password='d3v0lut10n')
            gl = gitlab.Gitlab('http://13.59.58.199/gitlab', private_token='NpsUxU6TamQB7GzgCYyu')

            # make an API request to create the gl.user object. This is mandatory if you use the username/password authentication.
            gl.auth()


            print("\n\n====================== Groups ======================")
            groups = []
            for group in gl.groups.list():
                print(group)
                groups.append(group.attributes)

            groupsdict = {"groups":groups}
            print(json.dumps(groupsdict, indent=4, sort_keys=True))


            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                groups=groupsdict,
                test="test message"
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()


        except:
            self.write({"success":False})



