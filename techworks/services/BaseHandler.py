#!/usr/bin/python3


import tornado.web


# Universal WebSocket
from techworks.services.websocket.WebSocketConnection import WebSocketConnection

# Base Tornado  Handler : All handlers should  inherit from Parent
class BaseHandler(tornado.web.RequestHandler):
    @property
    def websocket(self):
        return WebSocketConnection.participants

    # Turning off annoying CORS "Cross-Site Access-Origin" errors (for now..)
    def set_default_headers(self):
        print ("Setting Cross Origin Policy Headers!!!")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with, content-type, accept")
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')

