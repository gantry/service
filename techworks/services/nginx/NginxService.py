#!/usr/bin/python3

import json
import simplejson



from techworks.services.BaseHandler import BaseHandler
from techworks.drivers.NginxDriver import NginxDriver

import tornado.web
import tornado.escape
import tornado.httpclient


# ============================================
# Service which proxies calls to GitLab API
# ============================================
class NginxService(BaseHandler):



    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            print ("Executing Command")
            command = data["command"]
            print (command)
            container = data["container"]
            print (container)



            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:
            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')

            nginxDriver = NginxDriver()

            # Load JsonConfigs from File
            jsonConfig = nginxDriver.getJsonConfig()

            # Save NGINX Config to File
            nginxDriver.saveNginxConfig(jsonConfig)


            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                proxyList=jsonConfig,
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()


        except:
            self.write({"success": False})

