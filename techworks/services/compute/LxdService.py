#!/usr/bin/python3

import json
import simplejson



from techworks.services.BaseHandler import BaseHandler
from techworks.drivers.AnsibleDriver import AnsibleDriver
from techworks.drivers.LxdDriver import LxdDriver
from techworks.drivers.NginxDriver import NginxDriver

import techworks.drivers.LxcDriver as lxc

import tornado.web
import tornado.escape
import tornado.httpclient


# ============================================
# Service which proxies calls to GitLab API
# ============================================
class LxdService(BaseHandler):



    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)
            print ("Data")
            print (data)

            # HostsFile
            print ("Executing Command")
            command = data["command"]
            print (command)
            container = data["container"]
            print (container)


            # Command Dispatcher
            if command == "create":
                lxc.create(container)
            elif command == "start":
                lxc.start(container)
            elif command == "stop":
                lxc.stop(container)
            elif command == "destroy":
                lxc.destroy(container)
            elif command == "info":
                lxc.info(container)

            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:
            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')


            ########################################
            # Testing New API Based Driver
            # lxd = LxdDriver()
            # lxd.run("testName")
            ########################################

            # Call LXDDriver to trasmit LXD Meta-Data to Dashboard
            ansible = AnsibleDriver()

            print('ListX Call')
            listx = lxc.listx()
            containers_all = []

            print("==============  GetPlaybookList  =================")
            # playbooks = ["install-common.yml", "install-java.yml", "install-ansible.yml"]
            playbooks = ansible.getPlaybookList()
            print(playbooks)

            for status in ['RUNNING', 'FROZEN', 'STOPPED']:
                containers_by_status = []

                running = (status == 'RUNNING')
                for container in listx[status]:
                    settings = {}

                    if running:
                        print ("Getting IPV4")
                        ipAddress = lxc.ip_address(container, running)
                        print (ipAddress)
                        settings['ipv4'] = ipAddress
                        settings['flags'] = "up"
                        print (settings['ipv4'])

                    # print ("Append Name, Mem Usage, & Settings")
                    containers_by_status.append({
                        'name': container,
                        # 'memusg': sysInfo.memory_usage(container),
                        'settings': settings
                    })

                containers_all.append({
                    'status': status.lower(),
                    'containers': containers_by_status
                })

            # print('CheckUbuntu')
            # print(sysInfo.check_ubuntu())

            nginxDriver = NginxDriver()

            # Load JsonConfigs from File
            nginxJsonConfig = nginxDriver.loadJsonConfig()

            # Save NGINX Config to File
            nginxDriver.saveNginxConfig(nginxJsonConfig)


            print('lxc.ls()')
            print(lxc.ls())

            print('containers_all')
            print(containers_all)

            # terraformOutput = terraformTest()


            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                containers=lxc.ls(),
                containers_all=containers_all,
                playbooks=playbooks,
                proxyList=nginxJsonConfig,
                test="test message"
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()


        except:
            self.write({"success": False})

