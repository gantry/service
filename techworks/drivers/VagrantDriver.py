
#!/usr/bin/python3
import os
import os.path
import vagrant
from vagrant import compat
import subprocess
# from subprocess import Popen, PIPE
from subprocess import call


class VagrantDriver() :
    # ==================================================
    # Vars
    # ==================================================

    hostsFile = "./.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory"
    #hostsFile = "./hosts"

    varsFile = "./vars/user_vars.yml"
    varsFileExample = "./vars/user_vars-example.yml"


    def installVagrant(self):
        os.system("./init-metal.sh")

    def getVagrant(self):
        # Create Vagrant Interface (set verbosity)
        # v = vagrant.Vagrant()
        v = vagrant.Vagrant(quiet_stdout=False, quiet_stderr=False)
        return v

    def executeVagrantCommand(self, operation, machine_name=None, snapshot_name=None):

        msg = ""

        # Status
        if operation == "list":
            msg = self.list_box_names()
            msg += self.list_vms()
            msg += self.list_playbooks()

        elif operation == "status":
            msg = self.status(machine_name)

        # Create/Provision/Destroy
        elif operation == "up":
            msg = self.up(machine_name)
        elif operation == "provision":
            msg = self.provision(machine_name)
        elif operation == "halt":
            msg = self.halt(machine_name)
        elif operation == "destroy":
            msg = self.destroy(machine_name)

        # Suspend/Resume
        elif operation == "suspend":
            msg = self.suspend(machine_name)
        elif operation == "resume":
            msg = self.resume(machine_name)

        # Snapshots
        elif operation == "snapshot_list":
            msg = self.snapshot_list(machine_name)
        elif operation == "snapshot_push":
            msg = self.snapshot_push()
        elif operation == "snapshot_pop":
            msg = self.snapshot_pop()
        elif operation == "snapshot_save":
            msg = self.snapshot_save(snapshot_name, machine_name)
        elif operation == "snapshot_restore":
            msg = self.snapshot_restore(snapshot_name, machine_name)
        elif operation == "snapshot_delete":
            msg = self.snapshot_delete(snapshot_name, machine_name)


        # Config
        elif operation == "ssh_config":
            msg = self.ssh_config(machine_name)
        elif operation == "user":
            msg = self.user(machine_name)
        elif operation == "hostname":
            msg = self.hostname(machine_name)
        elif operation == "port":
            msg = self.port(machine_name)
        elif operation == "keyfile":
            msg = self.keyfile(machine_name)
        elif operation == "user_hostname":
            msg = self.user_hostname(machine_name)
        elif operation == "user_hostname_port":
            msg = self.user_hostname_port(machine_name)

        elif operation == "list_box_names":
            msg = self.list_box_names()
        elif operation == "list_vms":
            msg = self.list_vms()
        elif operation == "list_playbooks":
            msg = self.list_playbooks()
        else:
            print("Unknonw Operation : " + operation)

        print("Returning Message")
        print(msg)
        return msg

    # Status
    def status(self, machine_name=None):
        v = self.getVagrant()
        msg = v.status(vm_name=machine_name)
        print(msg)
        return msg

    # Create/Provision/Destroy
    def up(self, machine_name=None):
        v = self.getVagrant()
        msg = v.up(vm_name=machine_name)
        print(msg)
        return msg
    def provision(self, machine_name=None):
        v = self.getVagrant()
        msg = v.provision(vm_name=machine_name)
        print(msg)
        return msg
    def halt(self, machine_name=None):
        v = self.getVagrant()
        msg = v.halt(vm_name=machine_name)
        print(msg)
        return msg
    def destroy(self, machine_name=None):
        v = self.getVagrant()
        msg = v.destroy(vm_name=machine_name)
        print(msg)
        return msg

    # Suspend/Resume
    def suspend(self, machine_name=None):
        v = self.getVagrant()
        msg = v.suspend(vm_name=machine_name)
        print(msg)
        return msg
    def resume(self, machine_name=None):
        v = self.getVagrant()
        msg = v.resume(vm_name=machine_name)
        print(msg)
        return msg

    # Snapshots
    def snapshot_list(self, machine_name=None):
        v = self.getVagrant()
        msg = v.snapshot_list(machine_name)
        print(msg)
        return msg
    def snapshot_push(self):
        v = self.getVagrant()
        msg = v.snapshot_push()
        print(msg)
        return msg
    def snapshot_pop(self):
        v = self.getVagrant()
        msg = v.snapshot_pop()
        print(msg)
        return msg
    def snapshot_save(self, snapshot_name, machine_name=None):
        v = self.getVagrant()
        msg = v.snapshot_save(snapshot_name, machine_name)
        print(msg)
        return msg
    def snapshot_restore(self, snapshot_name, machine_name=None):
        v = self.getVagrant()
        msg = v.snapshot_restore(snapshot_name, machine_name)
        print(msg)
        return msg
    def snapshot_delete(self, snapshot_name, machine_name=None):
        v = self.getVagrant()
        msg = v.snapshot_delete(snapshot_name, machine_name)
        print(msg)
        return msg


    # Config
    def ssh_config(self, machine_name=None):
        v = self.getVagrant()
        msg = v.ssh_config(vm_name=machine_name)
        print(msg)
        return msg
    def user(self, machine_name=None):
        v = self.getVagrant()
        msg = v.user(vm_name=machine_name)
        print(msg)
        return msg
    def hostname(self, machine_name=None):
        v = self.getVagrant()
        msg = v.hostname(vm_name=machine_name)
        print(msg)
        return msg
    def port(self, machine_name=None):
        v = self.getVagrant()
        msg = v.port(vm_name=machine_name)
        print(msg)
        return msg
    def keyfile(self, machine_name=None):
        v = self.getVagrant()
        msg = v.keyfile(vm_name=machine_name)
        print(msg)
        return msg
    def user_hostname(self, machine_name=None):
        v = self.getVagrant()
        msg = v.user_hostname(vm_name=machine_name)
        print(msg)
        return msg
    def user_hostname_port(self, machine_name=None):
        v = self.getVagrant()
        msg = v.user_hostname_port(vm_name=machine_name)
        print(msg)
        return msg

    # ==================================================
    # Operations
    # ==================================================

    def list_box_names(self):
        print("\nCurrent Available Boxes : ")
        listing = compat.decode(subprocess.check_output('vagrant box list --machine-readable', shell=True))
        # print(listing)

        box_names = []
        for line in listing.splitlines():
            # Vagrant 1.8 added additional fields to the --machine-readable output,
            # so unpack the fields according to the number of separators found.
            if line.count(',') == 3:
                timestamp, _, kind, data = line.split(',')
            else:
                timestamp, _, kind, data, extra_data = line.split(',')
            if kind == 'box-name':
                box_names.append(data.strip())

        print(box_names)
        return box_names

    def list_vms(self):
        print("\nCurrent Available VMs : ")
        listing = compat.decode(subprocess.check_output('vagrant status --machine-readable', shell=True))
        print(listing)

        box_names = []
        for line in listing.splitlines():
            if line.count('metadata') > 0:
                timestamp, name, kind, type, vendor = line.split(',')
                box_names.append(name.strip())

        print(box_names)
        return box_names

    def list_playbooks(self):
        print("\nCurrent Available Playbooks : ")
        listing = compat.decode(subprocess.check_output('ls ../ansible/playbooks', shell=True))
        # print(listing)

        playbook_names = []
        for line in listing.splitlines():
            if line.count('.yml') > 0:
                playbook_names.append(line)

        print(playbook_names)
        return playbook_names


    # # Run Ansible Playbook (drop to CLI)
    # def runPlaybook(self, names, playbooks):
    #     for playbook in playbooks:
    #         for name in names:
    #             path = "playbooks/" + playbook
    #             playbookCommand = "ansible-playbook -vvvv " + path + " -i " + self.hostsFile + " -l " + name
    #             print(playbookCommand )
    #
    #             os.system(playbookCommand)
    #     return;
