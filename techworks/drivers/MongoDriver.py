#!/usr/bin/python3

import pymongo
from pymongo import MongoClient

from mongoengine import *
connect('mongoengine_test', host='localhost', port=27017)

class MongoDriver() :

    def connect(self):
        # client = MongoClient()
        client = MongoClient('localhost', 27017)

    # def accessDatabase(self):
        db = client.pymongo_test
        dbclone = client['pymongo_test']

    # def insertOne(self):
        posts = db.posts
        post_data = {
            'title': 'Python and MongoDB',
            'content': 'PyMongo is fun, you guys',
            'author': 'Scott'
        }
        result = posts.insert_one(post_data)
        print('One post: {0}'.format(result.inserted_id))

    # def insertList(self):
        post_1 = {
            'title': 'Python and MongoDB',
            'content': 'PyMongo is fun, you guys',
            'author': 'Scott'
        }
        post_2 = {
            'title': 'Virtual Environments',
            'content': 'Use virtual environments, you guys',
            'author': 'Scott'
        }
        post_3 = {
            'title': 'Learning Python',
            'content': 'Learn Python, it is easy',
            'author': 'Bill'
        }
        new_result = posts.insert_many([post_1, post_2, post_3])
        print('Multiple posts: {0}'.format(new_result.inserted_ids))


    # def findOne(self):
        bills_post = posts.find_one({'author': 'Bill'})
        print(bills_post)

    # def findList(self):
        scotts_posts = posts.find({'author': 'Scott'})
        print(scotts_posts)
        for post in scotts_posts:
            print(post)